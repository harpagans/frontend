import {Component, NgZone} from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  environments: any[] = [];

  updateEnvironments(incomingData: any) {
    let environmentData = _.cloneDeep(incomingData);

    _.remove(this.environments, {"name": environmentData.name});
    this.environments.push(environmentData);
    _.sortBy(this.environments, ['name']);

    environmentData.status = _.reduce(
      _.flatMap(
        _.flatMap(environmentData.groups, group => group.services),
        service => service.status),
      (status1, status2) => status1 == 'UP' && status2 == 'UP' ? 'UP' : 'DOWN');
  }

  constructor() {
    const es = new window['EventSource']('/api/status/');

    let zone = new NgZone({enableLongStackTrace: false});
    es.onmessage = ev => {
      zone.run(() => this.updateEnvironments(JSON.parse(ev.data)));
    };

    // this.updateEnvironments({
    //   name: 'stage',
    //   groups: [
    //     {
    //       name: 'resource1',
    //       services: [
    //         {
    //           name: 'service1',
    //           status: 'UP',
    //           version: '10.4.2'
    //         }
    //       ]
    //     },
    //     {
    //       name: 'resource2',
    //       services: [
    //         {
    //           name: 'service2',
    //           status: 'UP',
    //           version: '10.4.2'
    //         }
    //       ]
    //     }
    //   ]
    // });
    //
    // this.updateEnvironments({
    //   name: 'preprod',
    //   groups: [
    //     {
    //       name: 'resource3',
    //       services: [
    //         {
    //           name: 'service3',
    //           status: 'UP',
    //           version: '10.4.2'
    //         }
    //       ]
    //     },
    //     {
    //       name: 'resource4',
    //       services: [
    //         {
    //           name: 'service4',
    //           status: 'TO',
    //           version: '10.4.1'
    //         }
    //       ]
    //     }
    //   ]
    // });
  }
}
